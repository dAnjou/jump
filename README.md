# Jump

My first game ever. It's built on top of [PixiJS](http://www.pixijs.com).

I'd like to thank the creators of the various assets and tools I used:

- [Birds](https://opengameart.org/content/a-bald-eagle) by [Spring](https://opengameart.org/users/spring)
- [Parachute](https://www.deviantart.com/modernsorcery/art/Parachute-Bannedstory-custom-540970451) by [Modern Sorcery](https://www.deviantart.com/modernsorcery)
- [VT323 font](https://fonts.google.com/specimen/VT323) by Peter Hull
- Avatars made with [Avatars In Pixels](http://www.avatarsinpixels.com)
- Sprites made with [TexturePacker](https://www.codeandweb.com/texturepacker)
- Ground by [BayatGames](https://bayatgames.com/)
- Clouds by unknown artist

Honorable mentions: [Gimp](https://www.gimp.org), [RealFaviconGenerator](https://realfavicongenerator.net/), [Type for PIXI](https://github.com/abraaobuenotype/type)
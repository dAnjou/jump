import { contain, detect } from './collision.js'
import {
    createClouds,
    createControl,
    createHealth,
    createHeight,
    createGround,
    createStateButton,
    createVoucher,
    createMe,
    createMessage,
    Flock,
    Jumper
} from './stuff.js';

const JUMPER_SPEED = 10;
const FALL_SPEED = 3;
const HEIGHT = 4000;
const DAMAGE = 0.5;
const DIFFICULTY = 8;

let app = new PIXI.Application();
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
app.renderer.backgroundColor = 0x87cefa;
app.renderer.autoDensity = true;
app.renderer.resize(window.innerWidth, window.innerHeight);
document.body.appendChild(app.view);

var loader = new type.Loader();
loader.add("vt323", "fonts/vt323/vt323-v10-latin-regular.ttf");
loader.once('loadComplete', pixiLoad);
loader.load();

function pixiLoad() {
    PIXI.Loader.shared
        .add("sprites/faller.json")
        .add("sprites/parachute.json")
        .add("sprites/eagle.json")
        .add("background/cloud1.png")
        .add("background/cloud2.png")
        .add("background/cloud3.png")
        .add("background/cloud4.png")
        .add("background/ground.png")
        .add("voucher.png")
        .add("me.png")
        .load(setup);
}

let jumper,
    flock,
    healthText,
    health,
    heightText,
    height,
    leftControl,
    rightControl,
    clouds,
    ground,
    successes = 0,
    state;

function moveLeft() {
    jumper.vx = -1 * JUMPER_SPEED;
}

function moveRight() {
    jumper.vx = JUMPER_SPEED;
};

function stopMove() {
    jumper.vx = 0;
}

function jumperGroundDistance(top, bottom) {
    return bottom.y - top.y - top.height;
}

function reset() {
    jumper.init();

    ground.y = jumper.y + jumper.height + HEIGHT;

    health = 100;
    healthText.text = health;

    height = jumperGroundDistance(jumper, ground);
    heightText.text = height;

    flock.init();

    leftControl.visible = true;
    rightControl.visible = true;
}

function setup() {
    jumper = new Jumper(
        (e) => app.screen.width / 2 - e.width / 2,
        () => app.screen.height / 5
    );

    leftControl = createControl(app, {
        width: app.screen.width / 2 - 1,
        height: app.screen.height / 2,
        x: app.screen.x,
        y: app.screen.height - app.screen.height / 2,
        move: moveLeft,
        stop: stopMove
    });
    rightControl = createControl(app, {
        width: app.screen.width / 2 - 1,
        height: app.screen.height / 2,
        x: app.screen.width - app.screen.width / 2 + 1,
        y: app.screen.height - app.screen.height / 2,
        move: moveRight,
        stop: stopMove
    });

    clouds = createClouds(app);

    ground = createGround(app);

    flock = new Flock({
        width: app.screen.width,
        height: app.screen.height - ground.height
    }, DIFFICULTY, HEIGHT);

    health = 100;
    healthText = createHealth(app, health);

    height = HEIGHT;
    heightText = createHeight(app, height);

    reset();
    startState();

    app.ticker.add(delta => gameLoop(delta));
}

function gameLoop(delta) {
    heightText.x = app.screen.width - heightText.width - 20;
    state(delta);
}

function startState() {
    let startButton = createStateButton(app, "Start");
    startButton.on("pointertap", () => {
        app.stage.removeChild(startButton);

        app.stage.addChild(
            ...clouds,
            jumper,
            flock,
            ground,
            healthText,
            heightText,
            leftControl,
            rightControl
        );

        jumpingState();
    });
    app.stage.addChild(startButton);

    state = (delta) => { };
}

function pickSide(a, b) {
    let x = a.x + a.width / 2;
    let direction;
    if (x > app.screen.width / 2) {
        x = -1 - b.width;
        direction = 1;
    } else {
        x = 1 + app.screen.width;
        direction = -1;
    }
    return {
        x: x,
        direction: direction
    };
}

function successState() {
    leftControl.visible = false;
    rightControl.visible = false;
    let me = createMe();
    let message, messageH;
    const messageW = app.screen.width - 40;
    const messageX = 20;
    const messageY = app.screen.height - ground.height - me.height - 10;
    me.on("pointertap", () => {
        switch (successes) {
            case 0:
                me.interactive = false;
                [message, messageH] = createMessage(
                    messageW,
                    "That was pretty good but I want to see it again!"
                );
                message.x = messageX;
                message.y = messageY - messageH;
                message.on("pointertap", () => {
                    app.stage.removeChild(me);
                    app.stage.removeChild(message);
                    reset();
                    jumpingState();
                })
                app.stage.addChild(message);
                message.play();

                state = (delta) => { };
                break;

            case 1:
                me.interactive = false;
                [message, messageH] = createMessage(
                    messageW,
                    "I'm impressed! Here, I think you have better use for this than me: ..."
                );                
                message.x = messageX;
                message.y = messageY - messageH;
                message.on("pointertap", () => {
                    app.stage.removeChild(message);
                    const voucher = createVoucher(2, app.screen);
                    app.stage.addChild(voucher.sprite);

                    state = (delta) => {
                        voucher.rotate();
                    };
                })
                app.stage.addChild(message);
                message.play();

                state = (delta) => { };
                break;

            default:
                break;
        }
        successes += 1;
    });
    let side = pickSide(jumper, me);
    me.x = side.x;
    me.y = app.screen.height - ground.height - me.height;
    app.stage.addChild(me);

    state = (delta) => {
        if (!detect(jumper, me)) {
            me.x = me.x + 3 * side.direction;
        }
    };
}

function gameOverState() {
    let gameOverButton = createStateButton(app, "Game Over");
    gameOverButton.on("pointertap", () => {
        app.stage.removeChild(gameOverButton);
        reset();
        jumpingState();
    });
    app.stage.addChild(gameOverButton);

    state = (delta) => { };
}

function jumpingState() {
    state = (delta) => {
        contain(jumper, app.screen);
        jumper.x += jumper.vx;

        height = Math.round(jumperGroundDistance(jumper, ground));
        heightText.text = height;
        if (height <= 0) {
            heightText.text = 0;
            jumper.person.stop();
            jumper.person.interactive = false;
            if (jumper.parachuteDeployed) {
                jumper.openParachute.close();
                successState();
                return
            }
            gameOverState();
            return
        }

        flock.fly();

        let collidingBirds = flock.collidingBirds(jumper.collisionObject);
        health -= DAMAGE * collidingBirds;
        healthText.text = Math.floor(health);

        if (health <= 0) {
            gameOverState();
            return
        }

        if (ground.y <= (app.screen.height - ground.height + FALL_SPEED)) {
            jumper.y += FALL_SPEED;
        } else {
            ground.y -= FALL_SPEED;
        }
    };
}

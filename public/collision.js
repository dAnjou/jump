export function detect(r1, r2) {

    //Define the variables we'll need to calculate
    let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

    //hit will determine whether there's a collision
    hit = false;

    //Find the half-widths and half-heights of each sprite
    r1.halfWidth = r1.width / 2;
    r1.halfHeight = r1.height / 2;
    r2.halfWidth = r2.width / 2;
    r2.halfHeight = r2.height / 2;

    //Find the center points of each sprite
    r1.centerX = r1.getGlobalPosition().x + r1.halfWidth;
    r1.centerY = r1.getGlobalPosition().y + r1.halfHeight;
    r2.centerX = r2.getGlobalPosition().x + r2.halfWidth;
    r2.centerY = r2.getGlobalPosition().y + r2.halfHeight;

    //Calculate the distance vector between the sprites
    vx = Math.abs(r1.centerX - r2.centerX);
    vy = Math.abs(r1.centerY - r2.centerY);

    //Figure out the combined half-widths and half-heights
    combinedHalfWidths = r1.halfWidth + r2.halfWidth;
    combinedHalfHeights = r1.halfHeight + r2.halfHeight;

    if (vx < combinedHalfWidths && vy < combinedHalfHeights) {
        hit = true;
    } else {
        hit = false;
    }

    return hit;
};

export function contain(sprite, container) {
    if (sprite.x <= container.x) {
        sprite.vx = 0;
        sprite.x += 1;
    }
    if (sprite.x + sprite.width >= container.width) {
        sprite.vx = 0;
        sprite.x -= 1;
    }
}

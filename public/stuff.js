import { detect } from './collision.js'

let rsrc = PIXI.Loader.shared.resources;

function loadAnimatedSprite(animation, speed) {
    let sprite = new PIXI.AnimatedSprite(animation);
    sprite.animationSpeed = speed;
    return sprite;
}

export class Jumper extends PIXI.Container {
    constructor(x, y) {
        super();

        this.person = loadAnimatedSprite(
            rsrc["sprites/faller.json"].spritesheet.animations["faller"],
            0.5
        );
        this.openParachute = loadAnimatedSprite(
            rsrc["sprites/parachute.json"].spritesheet.animations["open"],
            0.4
        );
        this.closedParachute = loadAnimatedSprite(
            rsrc["sprites/parachute.json"].spritesheet.animations["closed"],
            0.4
        );

        this.closedParachute.loop = false;
        this.closedParachute.width *= 3.1;
        this.closedParachute.height *= 3.1;
        this.closedParachute.name = "closedParachute";
        this.closedParachute.open = this.closedParachute.play;
        this.closedParachute.onComplete = () => {
            this.openParachute.renderable = true;
            this.closedParachute.gotoAndStop(0);
            this.collisionObject = this;
            this.parachuteDeployed = true;
            this.person.animationSpeed = 0.1;
        };

        this.openParachute.loop = false;
        this.openParachute.width *= 3.1;
        this.openParachute.height *= 3.1;
        this.openParachute.name = "openParachute";
        this.openParachute.close = this.openParachute.play;
        this.openParachute.onComplete = () => {
            this.openParachute.renderable = false;
            this.openParachute.gotoAndStop(0);
            this.collisionObject = this.person;
            this.parachuteDeployed = false;
            this.person.stop();
        };

        this.person.name = "person";
        this.person.on("pointertap", () => {
            if (!this.parachuteDeployed) {
                this.closedParachute.open();
            }
        });

        this.addChild(
            this.openParachute,
            this.closedParachute,
            this.person
        );

        this.initPosition = {
            x: x(this),
            y: y()
        };
    }

    init() {
        this.position = this.initPosition;
        this.vx = 0;
        this.vy = 0;
        this.person.interactive = true;
        this.person.play();
        this.person.y = 150;
        this.person.animationSpeed = 0.5;
        this.collisionObject = this.person;
        this.parachuteDeployed = false;
        this.openParachute.renderable = false;
    }
}

export function createClouds(app) {
    let clouds = [];
    [3, 1, 2, 1, 4, 1, 3].forEach((value, index, array) => {
        let cloud = new PIXI.Sprite(rsrc["background/cloud" + value + ".png"].texture);
        let breakpoint = app.screen.width / (array.length + 0) * (index + 0);
        cloud.x = breakpoint;
        cloud.y = 10;
        cloud.name = "cloud";
        if (index % 2 !== 0) cloud.y += 20;
        clouds.push(cloud);
    });
    return clouds;
}

export function createControl(app, options) {
    let control = new PIXI.Graphics()
        .beginFill()
        .drawRect(0, 0, options.width, options.height)
        .endFill();
    control.renderable = false;
    control.x = options.x;
    control.y = options.y;
    control.interactive = true;
    control.on('pointerdown', options.move);
    control.on('pointerup', options.stop);
    control.on('pointerupoutside', options.stop);
    return control;
}

const baseStyle = {
    fontFamily: "vt323",
    dropShadow: true,
    dropShadowAngle: 0.5,
    fill: "red",
    fontSize: 120,
    fontWeight: "bold",
    letterSpacing: 5,
    padding: 10,
    strokeThickness: 10
};

export function createMessage(width, text) {
    let style = Object.create(baseStyle);
    style.fontSize = 80;
    style.wordWrap = true;
    style.wordWrapWidth = width;
    style = new PIXI.TextStyle(style);
    let animation = [];
    let height = 0;
    for (var i = 0; i <= text.length; i++) {
        let subs = new PIXI.Text(text.substring(0, i), style);
        subs.updateText(true);
        height = Math.max(height, subs.height);
        animation.push(subs.texture);
    }
    let animatedText = new PIXI.AnimatedSprite(animation);
    animatedText.animationSpeed = 0.5;
    animatedText.loop = false;
    animatedText.interactive = true;
    return [animatedText, height];
}

export function createHealth(app, health) {
    let style = Object.create(baseStyle);
    let text = new PIXI.Text(health, new PIXI.TextStyle(style));
    text.x = 20;
    text.y = 20;
    return text;
}

export function createHeight(app, height) {
    let style = Object.create(baseStyle);
    style.align = "right";
    let text = new PIXI.Text(height, new PIXI.TextStyle(style));
    text.x = app.screen.width - text.width - 10;
    text.y = 20;
    return text;
}

export function createStateButton(app, label) {
    let style = Object.create(baseStyle);
    let text = new PIXI.Text(label, new PIXI.TextStyle(style));
    text.x = app.screen.width / 2 - text.width / 2;
    text.y = app.screen.height / 2 - text.height / 2;
    text.interactive = true;
    return text;
}

export function createGround(app) {
    let ground = rsrc["background/ground.png"].texture;
    let tilingGround = new PIXI.TilingSprite(
        ground,
        app.screen.width,
        ground.height
    );
    tilingGround.x = 0;
    return tilingGround;
}

export function createMe() {
    let me = rsrc["me.png"].texture;
    let sprite = new PIXI.Sprite(me);
    sprite.interactive = true;
    return sprite;
}

export function createVoucher(startWidth, container) {
    let voucher = rsrc["voucher.png"].texture;
    let sprite = new PIXI.projection.Sprite2d(voucher);
    let step = 0;
    let scaleVoucher;
    let shouldScaleVoucher;

    sprite.width = startWidth;
    sprite.scale.y = sprite.scale.x;

    if (container.width / container.height > sprite.width / sprite.height) {
        shouldScaleVoucher = (value) => sprite.height <= value;
        scaleVoucher = (value) => {
            sprite.height += value;
            sprite.scale.x = sprite.scale.y;
        }
        //sprite.x = app.screen.width / 2 - sprite.width / 2;
    } else {
        shouldScaleVoucher = (value) => sprite.width <= value;
        scaleVoucher = (value) => {
            sprite.width += value;
            sprite.scale.y = sprite.scale.x;
        }
        //sprite.y = app.screen.height / 2 - sprite.height / 2;
    }   

    sprite.x = container.width / 2;
    sprite.y = container.height;

    let factor = (sprite.y - container.height / 2) / container.width; 

    sprite.anchor.set(0.5, 0.5);
    sprite.proj.affine = PIXI.projection.AFFINE.AXIS_X;

    return {
        sprite: sprite,
        rotate: () => {
            if (shouldScaleVoucher(container.width)) {
                scaleVoucher(5);
                sprite.y -= 5 * factor;
                step += 1;
                sprite.rotation = step * 0.3;
            } else {
                sprite.rotation = 0;
            }
        }
    };
}

class Bird extends PIXI.AnimatedSprite {
    constructor(...args) {
        super(...args);
        this.animationSpeed = 0.2;
        this.name = "bird";
        this.width *= 4;
        this.height *= 4;
        this.play();
    }

    fly(vx, vy) {
        this.x = this.x + vx;
        this.y = this.y - vy;
    }
}

export class Flock extends PIXI.Container {
    constructor(container, amountPerSide, height) {
        super();
        this.container = container;
        this.amount = amountPerSide;
        this.gap = height / amountPerSide;
        this.vy = () => 2 + Math.random();
    }

    init() {
        this.removeChildren();
        const offset = 0.1 * this.gap;
        for (let index = 0; index < this.amount; index++) {
            const gap = index * this.gap;
            const y = this.container.height + gap;
            this.createBird(
                (e) => -1 - e.width - gap,
                y - offset,
                2,
                this.vy()
            );
            this.createBird(
                () => 1 + this.container.width + gap,
                y + offset,
                -2,
                this.vy()
            );
        }
    }

    createBird(x, y, vx, vy) {
        let bird = new Bird(
            rsrc["sprites/eagle.json"].spritesheet.animations["eagle"],
        );
        bird.x = x(bird);
        bird.vx = vx;
        bird.y = y;
        bird.vy = vy;
        this.addChild(bird);
        return bird;
    }

    fly() {
        for (const bird of this.children) {
            bird.fly(bird.vx, bird.vy);
        }
    }

    collidingBirds(jumper) {
        let amount = 0;
        for (const bird of this.children) {
            if (detect(jumper, bird)) {
                amount++;
                bird.tint = 0xff3300;
            } else {
                bird.tint = 0xFFFFFF;
            }
        }
        return amount;
    }
}
